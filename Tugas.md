# Book Web App

## Task Description
You need to create a simple web application that displays a table list of books obtained from a mock API. Your application should allow users to view book details and add or remove books from a list of favorites (stored locally). The list of favorite books doesn't need to be presented visually. Use the stored data to show visually which book is in the favorites and which is not, for example, by using a heart icon with two states. The book details should include the book title, author, description, cover image, and publication date.

## Technical Requirements
- Use Vue.js for the web app and Vuetify for the components.
- Handle the communication with the API using fetch or a library of your choice.
- Don't present all the books on one page, rather, use pagination and show 5 books per page.
- The list of book favorites should persist even after the user closes and re-opens the browser.
- Filter By Favorite book

## API
You can use a mock API that returns book data. Here's an example of the API endpoints to call:

### List
GET https://my-json-server.typicode.com/aditgege/mock/books

The response will be an array of objects, where each object represents a book and contains the following information:
- `id`: int - the ID of the book
- `title`: string - the title of the book.
- `author`: string - the name of the author.
- `description`: string - a short description of the book.
- `cover`: string - a URL pointing to the cover image of the book
- `publicationDate`: string - the publication date of the book, in ISO format.

### Detail
GET https://my-json-server.typicode.com/aditgege/mock/books/id

NOTE: Replace id by the actual numerical id of the book.
The response will be a book object, and contains the following information:
- `id`: int - the ID of the book
- `title`: string - the title of the book.
- `author`: string - the name of the author.
- `description`: string - a short description of the book
- `cover`: string - a URL pointing to the cover image of the book.
- `publicationDate`: string - the publication date of the book, in ISO format.

## Evaluation Criteria
We will evaluate your task based on the following criteria:
- Code quality: the code should be clean, easy to read, and follow best practices.
- Functionality: the application should meet the requirements listed above.
