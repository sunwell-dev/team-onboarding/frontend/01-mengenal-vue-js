# Mengenal Vue Js

## Daftar Isi
- [Tujuan](#tujuan)
- [Yang Dipelajari](#yang-dipelajari)
- [Ukuran Pencapaian](#ukuran-pencapaian)
- [Mengenal Vue Js](#mengenal-vue-js)
- [Bacaan Lanjut](#bacaan-lanjut)

## Tujuan <a name="tujuan"></a>

Tujuan dari materi ini adalah untuk memperkenalkan Vue Js kepada karyawan baru, serta memberikan pemahaman dasar mengenai framework ini.

## Yang Dipelajari <a name="yang-dipelajari"></a>
- Pengenalan singkat tentang Vue Js.
    - 1) Single file components (HTML, Javascript, CSS) dalam sebuah file        -> Options API & Composition API
    - 2) Template syntax
    - 3) Event handling
    - 4) Lifecycle hooks
    - 5) Watchers
    - 6) Template refs
    - 7) Props
    - 8) Async components
    - 9) Routing
- Konsep dasar Vue Js seperti data binding, directive, dan komponen.
- Cara menggunakan Vue Js dalam pengembangan web aplikasi.
- Contoh penggunaan Vue Js dalam proyek nyata.

## Ukuran Pencapaian <a name="ukuran-pencapaian"></a>

Karyawan diharapkan dapat:
- Memahami konsep dasar Vue Js.
- Mengimplementasikan Vue Js dalam pengembangan aplikasi web sederhana.
- Memiliki pemahaman yang cukup untuk memperluas pengetahuan mereka tentang Vue Js.

## Mengenal Vue Js <a name="mengenal-vue-js"></a>

Vue Js adalah sebuah framework JavaScript yang digunakan untuk membangun antarmuka pengguna yang interaktif. Ia menawarkan berbagai fitur yang memudahkan pengembangan aplikasi web, termasuk sistem pembaruan data yang reaktif dan kemampuan mengorganisir kode dengan baik melalui komponen-komponen.

## Bacaan Lanjut <a name="bacaan-lanjut"></a>

Untuk informasi lebih lanjut tentang Vue Js, Anda dapat membaca dokumentasinya di [Vue.js Documentation](https://vuejs.org/).

- [Vue.js Style Guide](https://vuejs.org/style-guide)
- [Vue.js Style Guide v2 options api](https://v2.vuejs.org/v2/style-guide/?redirect=true)
